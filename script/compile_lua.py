#!/usr/bin/python
# coding=utf-8

src = '../client'
outPath = '../ciphercode'
srcTemp = '../client_temp'
innerOutPath = src + '/ciphercode'
import os

def back_up_src():
    if os.path.exists(innerOutPath+'/'):
        os.system('sudo rm -r -f ' + innerOutPath)

    if os.path.exists(srcTemp):
	    print srcTemp + ' 文件夹授权'
	    os.system('sudo chmod -R 777 ' + srcTemp)
	    print '删除 ' + srcTemp
	    os.system('sudo rm -r -f ' + srcTemp)
    print '创建备份文件夹 ' + srcTemp
    os.mkdir(srcTemp)
    print '备份源代码'
    os.system('sudo cp -r -f ' + src + '/ ' + srcTemp +'/')

def cp_files():
    if os.path.exists(outPath):
        print outPath + ' 文件夹授权'
        os.system('sudo chmod -R 777 ' + outPath)
        print '删除 ' + outPath
        os.system('sudo rm -r -f ' + outPath + '/')
    print '创建输出文件夹 ' + outPath
    os.mkdir(outPath)
   
    print '拷贝所以资源与源代码到输出文件'
    os.system('sudo cp -r -f ' + srcTemp + '/ ' + outPath +'/')

def del_lua_files(path):
    for root , dirs, files in os.walk(path):
        for name in files:
            if name.endswith(".lua"):
                os.remove(os.path.join(root, name))
                print ("Delete File: " + os.path.join(root, name))

def del_base_files(path):
    print '删除 ' + path
    os.system('sudo rm -r -f ' + path)

def compile_lua():
    print '编译luac'
    os.system('sudo cocos luacompile -s ' + src +'/ -d ' + outPath + '/')
    print '创建' + innerOutPath
    os.mkdir(innerOutPath+'/')
    print '拷贝 ' + outPath + ' 到 ' + innerOutPath
    os.system('sudo cp -r -f ' + outPath + '/* ' +  innerOutPath + '/')

if __name__ == "__main__":
    back_up_src()
    cp_files()
    del_lua_files(outPath)
    compile_lua()
    del_base_files(innerOutPath + '/base/')
