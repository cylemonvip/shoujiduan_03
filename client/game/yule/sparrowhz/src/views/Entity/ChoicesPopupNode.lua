local cmd = appdf.req(appdf.GAME_SRC.."yule.sparrowhz.src.models.CMD_Game")
local GameLogic = appdf.req(appdf.GAME_SRC.."yule.sparrowhz.src.models.GameLogic")

local ChoicesPopupNode = class("ChoicesPopupNode", function ()
	return cc.Node:create()
end)

local CSB_RES_PATH = cmd.RES_PATH .. "ChoicesPopupNode.csb"

function ChoicesPopupNode:ctor(opType, aChoice)
	print("opType = " .. opType)
	self.aChoice = aChoice
	self.opType = opType
	self:initCsb()
	self:initChoice()
end

function ChoicesPopupNode:initCsb()
	self.rootNode =  cc.CSLoader:createNode(cmd.RES_PATH.."game/GameScene.csb")
	self.bgLayout = self.rootNode:getChildByName("BgLayout")
end

function ChoicesPopupNode:init()
	--根据当前数据，绘制对应的麻将
	for i, value in ipairs(self.aChoice) do
		local value, color  = GameLogic.GetNValueAndNColorByCardIndex(GameLogic.SwitchToCardIndex(value))
		local strFile = cmd.RES_PATH.."game/font_middle/font_"..nColor.."_"..nValue..".png"
		local font = display.newSprite(strFile)
			:move((i - 1) * 90, 0)
			:addTo(self.rootNode)
	end
end


return ChoicesPopupNode