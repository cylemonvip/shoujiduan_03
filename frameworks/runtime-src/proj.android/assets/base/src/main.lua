
cc.FileUtils:getInstance():setPopupNotify(false)
cc.FileUtils:getInstance():addSearchPath("base/src/")
cc.FileUtils:getInstance():addSearchPath("base/res/")
cc.FileUtils:getInstance():addSearchPath("client/res/")
cc.FileUtils:getInstance():addSearchPath("game/yule/sparrowhz/res")
cc.FileUtils:getInstance():addSearchPath("client/")
cc.FileUtils:getInstance():addSearchPath("client/src/privatemode/plaza/res")
cc.FileUtils:getInstance():addSearchPath("client/src/privatemode/game/yule/sparrowhz/res")

require "config"
require "cocos.init"

local function main()
    
    require("app.MyApp"):create():run()
end

local status, msg = xpcall(main, __G__TRACKBACK__)
if not status then
    print(msg)
end
